export const CURRENCY_FORMATTER =
    new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' })
