/* eslint-disable no-throw-literal */
import { DataLoans } from "./loansType";

export const addInvestment = (loanId: string, investSum: number, dataLoans: DataLoans) => {

  const loan = dataLoans.loans.find((loan) => loan.id === loanId)
  if (loan === undefined) {
    throw "could not find loan by id"
  }
  loan.available = (+loan.available - investSum).toString()
  loan.amount = (+loan.amount + investSum).toString()
  
  const newDataLoans = { loans: [...dataLoans.loans] }
  return newDataLoans
}