export interface Loan {
    id: string;
    title: string;
    tranche: string;
    available: string;
    annualized_return: string;
    term_remaining: string;
    ltv: string;
    amount: string;
}

export interface DataLoans {
    loans: Loan[];
}
