export const howDaysLeft = (unixNum: number): number => {
    const diff = unixNum - Date.now();
    return Math.round(diff / (1000 * 60 * 60 * 24));
};