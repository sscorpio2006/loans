import { DataLoans } from "./loansType";

const availableSum = (dataLoans: DataLoans) => {
    const sum = dataLoans.loans.reduce((sum, loan) => sum + +loan.available, 0);
    
    return Math.floor(sum) 
}

export default availableSum;

