import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Investment from './components/Investment';
import LoanComponent from './components/LoanComponent';
import ModalRoot from './components/ModalRoot';
import { DataLoans, Loan } from './utilities/loansType';
import { addInvestment } from './utilities/addInvestment';
import availableSum from './utilities/amountAvailable';
import { CURRENCY_FORMATTER } from './utilities/constants';
import { getDataLoans, updateDataLoans } from './services/API';

const Title = styled.h1`
  font-size: 30px;
  font-weight: 700;
  margin-bottom: 40px;
`;

const Wrapper = styled.div`
  width: 100%;
  height: 100vh ;
  background: #000;
  display: flex ;
  align-items: center;
  justify-content: center ;
`;

const MainBlock = styled.div`
  background: #d4d4d4;
  padding: 30px 40px;
`;

const AmountAvailable = styled.p`
  text-align: center ;
  font-size: 18px ;
`;

const App: React.FC = () => {
  const [dataLoans, setDataLoans] = useState<DataLoans>({ loans: [] });
  const [showModal, setShowModal] = useState<boolean>(false)
  const [currentLoan, setLoan] = useState<Loan>()
  const [invested, setInvested] = useState<{ [key: string]: boolean }>({});

  useEffect(() => {
    getDataLoans().then(data => setDataLoans({ loans: [...data] }))
  }, [])

  const makeInvesting = (loanId: string, investSum: number,) => {
    const newData = addInvestment(loanId, investSum, dataLoans)
    setDataLoans(newData)

    updateDataLoans(newData.loans.find((loan) => loan.id === loanId))

    const newInvest = { ...invested, [loanId]: true }
    setInvested(newInvest)
    setShowModal(false)
  }

  const totalAvailableSum = availableSum(dataLoans)

  return (
    <Wrapper>
      <MainBlock>
        <Title>Current Loans</Title>
        {dataLoans.loans.map((loan) =>
          <LoanComponent
            key={loan.id} loan={loan}
            onInvest={(loan) => {
              setLoan(loan)
              setShowModal(true)
            }}
            isInvested={invested[loan.id]}
          />
        )}

        <AmountAvailable>Total amount available for investments:
          <strong data-testid="total-amount"> {CURRENCY_FORMATTER.format(totalAvailableSum)}</strong>
        </AmountAvailable>
      </MainBlock>

      <ModalRoot isOpen={showModal} onClose={() => setShowModal(false)} >
        {currentLoan && <Investment
          currentLoan={currentLoan}
          makeInvesting={makeInvesting}
        />}
      </ModalRoot>
    </Wrapper>
  );
}

export default App;
