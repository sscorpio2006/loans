
import axios from 'axios';
import { Loan } from '../utilities/loansType';
axios.defaults.baseURL = "http://localhost:5000";

export async function getDataLoans() {
    try {
        return (await axios.get('/loans')).data;
    } catch (error) {
        console.error(error);
    }
}

export async function updateDataLoans(loan: Loan | undefined) {
    try {
        await axios.put(`/loans/${loan?.id}`, loan);
    } catch (error) {
        console.error(error);
    }
}
