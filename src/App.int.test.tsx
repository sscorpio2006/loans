import React from 'react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, screen, within, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from './App';

const server = setupServer(
  rest.get('http://localhost:5000/loans', (req, res, ctx) => {
    return res(
      ctx.json([
        {
          id: '1',
          title: 'MockTitle1',
          tranche: 'A',
          available: '10000',
          annualised_return: '8.60',
          term_remaining: '1648243234000',
          ltv: '48.80',
          amount: '80000',
        },
        {
          id: '5',
          title: 'MockTitle2',
          tranche: 'B',
          available: '20000',
          annualised_return: '7.10',
          term_remaining: '1651316711000',
          ltv: '48.80',
          amount: '100000',
        }
      ],
      ),
    );
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('App integration test', () => {
  it('should pass happy pass', async () => {
    render(<App />);
    const loanContainer = await screen.findByTestId('loanContainer-5');
    const loanBtn = within(loanContainer).getByRole('button', { name: /invest/i });
    userEvent.click(loanBtn);
    const input = await screen.findByTestId("field-sum");
    expect(input).toBeInTheDocument();

    userEvent.type(input, '3000');
    expect(input).toHaveValue(3000);

    const modalContainer = screen.getByTestId('modal-content');
    const submitBtn = within(modalContainer).getByRole('button', { name: /invest/i });
    userEvent.click(submitBtn);

    expect(await screen.findByTestId('modal-content')).not.toBeInTheDocument();

    const investedSign = within(loanContainer).queryByText('Invested');
    expect(investedSign).toBeInTheDocument();

    const loanAmount = within(loanContainer).getByTestId('loan-amount');
    const totalAmount = screen.getByTestId('total-amount');

    await waitFor(() => {
      expect(loanAmount).toHaveTextContent('£103,000.00');
    });
    await waitFor(() => {
      expect(totalAmount).toHaveTextContent('£27,000.00');
    });
  });
});