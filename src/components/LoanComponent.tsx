import styled from 'styled-components';
import { Loan } from '../utilities/loansType';
import { CURRENCY_FORMATTER } from '../utilities/constants';
import Btn from './Btn';


const Wrapper = styled.div`
  padding: 15px;
  margin: 20px 0;
  border: 1.5px solid grey;
  background: #f6f6f6;
`;

const Title = styled.h2`
  font-size: 20px;
  font-weight: 700;
  margin-bottom: 10px;
`;

const P = styled.p`
  font-size: 16px;
  padding: 2px;
`;

const DivSpaceBetween = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Box = styled.div`
  position: relative;
`
const Invested = styled.div`
  position: absolute;
  font-size: 18px;
  color: green;
  right: 10px;
  top: -5px;
`
const Button = styled(Btn)`
  margin-top: 25px;
`

interface Props {
  loan: Loan;
  onInvest: (loan: Loan) => void;
  isInvested: boolean;
}

const LoanComponent: React.FC<Props> = ({ loan, onInvest, isInvested }) => {
  const { title, tranche,
    available, annualized_return,
    amount } = loan;

  return (
    <Wrapper data-testid={'loanContainer-' + loan.id}>
      <Title>{title}</Title>
      <DivSpaceBetween>
        <div>
          <P >Tranche: {tranche}</P>
          <P>Available: {CURRENCY_FORMATTER.format(+available)}</P>
          <P>Annualized return: {annualized_return}%</P>
          <P data-testid={'loan-amount'}>Amount: {CURRENCY_FORMATTER.format(+amount)}</P>
        </div>

        <Box>
          {isInvested && <Invested>Invested</Invested>}
          <Button onClick={() => onInvest(loan)}>INVEST</Button>
        </Box>
      </DivSpaceBetween>

    </Wrapper >
  );
};

export default LoanComponent;
