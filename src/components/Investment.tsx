/* eslint-disable no-throw-literal */
import React from 'react';
import styled from 'styled-components';
import Btn from './Btn';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { Loan } from '../utilities/loansType';
import { howDaysLeft } from '../utilities/howDaysLeft';
import { CURRENCY_FORMATTER } from '../utilities/constants';

const Title = styled.h2`
  font-size: 20px;
  font-weight: 700;
  margin-bottom: 10px;
`;
const SubTitle = styled.h3`
  font-size: 18px;
  font-weight: 600;
  margin-bottom: 20px;
`;
const P = styled.p`
  font-size: 16px;
  padding: 2px ;
`;
const Paragraph = styled(P)`
  margin-bottom: 10px ;
`;
const Input = styled(Field)`
    align-items: center;
  height: 46px;
  border-radius: 8px;
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  line-height: 14px;
  border: 1px solid grey;
`;

const StyledErrorMessage = styled.div`
  font-size: 11px;
  line-height: 12px;
  color: red;
  margin-bottom: 14px;
`;

interface Props {
    currentLoan: Loan;
    makeInvesting: (loanId: string, sumForInvest: number,) => void;
}

const Investment: React.FC<Props> = ({ currentLoan, makeInvesting }) => {
    const { title, available, term_remaining } = currentLoan;

    const initialValues = {
        invest: '',
    };
    const FormSchema = Yup.object().shape({
        invest: Yup.number()
            .max(+available, 'You have entered a larger amount than is available for investment')
            .required('Required !')
            .positive('Must be a positive number')
            .integer('Please enter integer')
    });

    const onSubmit = (sumForInvest: number) => {
        makeInvesting(currentLoan.id, sumForInvest)
    }
    const daysLeft = howDaysLeft(+term_remaining)

    return (
        <>
            <Title>Invest in Loan</Title>
            <SubTitle>{title}</SubTitle>
            <P>Amount available: {CURRENCY_FORMATTER.format(+available)}</P>
            <Paragraph>Loan ends in: {daysLeft} days</Paragraph>
            <Paragraph>Investment amount: (£) </Paragraph>

            <Formik
                enableReinitialize
                initialValues={initialValues}
                validationSchema={FormSchema}
                onSubmit={({ invest }) => {
                    onSubmit(+invest);
                }}
            >
                {({ errors, touched, }) => (
                    <Form>
                        <>
                            <label htmlFor="invest"/>
                            <Input
                                id="invest"
                                data-testid="field-sum"
                                errors={errors.invest}
                                name="invest"
                                type="number"
                            />
                            <Btn
                                type="submit">INVEST</Btn>
                        </>
                        {errors.invest && touched.invest && (
                            <StyledErrorMessage>{errors.invest}</StyledErrorMessage>
                        )}
                    </Form>
                )}
            </Formik>
        </>
    );
};

export default Investment;