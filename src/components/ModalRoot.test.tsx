/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import ModalRoot from './ModalRoot';

const child = <h1>My portal</h1>

describe('Portal', () => {

    it('modal shows the children and a close button', () => {
        const clickMock = jest.fn();
        render(
            <ModalRoot
                isOpen={true} onClose={clickMock}>{child}
            </ModalRoot>
        );

        expect(screen.getByText('My portal')).toBeInTheDocument()

        userEvent.click(screen.getByRole('button', { name: /x/i }))
        expect(clickMock).toHaveBeenCalledTimes(1);

    });

    it('click on Wrapper: should be called onClose 1 times', () => {
        const clickMock = jest.fn();
        render(
            <ModalRoot
                isOpen={true} onClose={clickMock}>{child}
            </ModalRoot>
        );

        userEvent.click(screen.getByTestId("modal-wrapper"))
        expect(clickMock).toHaveBeenCalledTimes(1);
    });

    it('click on Box: should be called onClose 0 times', () => {
        const clickMock = jest.fn();
        render(
            <ModalRoot
                isOpen={true} onClose={clickMock}>{child}
            </ModalRoot>
        );

        userEvent.click(screen.getByTestId("modal-content"))
        expect(clickMock).toHaveBeenCalledTimes(0);
    });

    it('should be unmounted', () => {
        const clickMock = jest.fn();
        const { unmount } = render(
            <ModalRoot
                isOpen={true} onClose={clickMock}>{child}
            </ModalRoot>
        );
        expect(screen.getByText('My portal')).toBeInTheDocument()
        unmount()
        expect(screen.queryByText('My portal')).not.toBeInTheDocument()
    });
});
