import React from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';

const Wrapper = styled.div`
  position: fixed;
  z-index: 1;
  background: #0000009d;
  left: 0;
  top: 0;
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Box = styled.div`
  position: relative;
  background: #ffffff;
  padding: 20px;
`;

const Close = styled.button`
  position: absolute;
  right: 10px;
  top: 10px;
  font-size: 22px;
  background: none;
  border: none ;
  cursor: pointer;
`;

const modalRoot = document.createElement('div');
modalRoot.setAttribute("id", "modal-root");
document.body.appendChild(modalRoot);

interface Props {
  isOpen: boolean;
  onClose: (event: React.MouseEvent<HTMLElement>) => void;
}

const ModalRoot: React.FC<Props> = ({ isOpen, onClose, children }) => {
  if (!isOpen) {
    return null
  }

  return ReactDOM.createPortal(
    <Wrapper data-testid="modal-wrapper" onClick={(e) => {
      if (e.currentTarget === e.target) {
        onClose(e);
      }
    }} >
      <Box data-testid="modal-content">
        <Close onClick={onClose}>x</Close>
        {children}
      </Box>
    </Wrapper>,
    modalRoot,
  )
}

export default ModalRoot;