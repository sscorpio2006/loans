/* eslint-disable testing-library/no-wait-for-multiple-assertions */
/* eslint-disable react/react-in-jsx-scope */
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import Investment from './Investment';

describe('Investment component', () => {
    const funcMock = jest.fn();

    const loan = {
        "id": "1",
        "title": "Voluptate et sed tempora qui quisquam.",
        "tranche": "A",
        "available": "11959",
        "annualized_return": "8.60",
        "term_remaining": "1656396208000",
        "ltv": "48.80",
        "amount": "85754",
        "isInvested": false
    };

    it('should be render -> happy path', () => {

        render(
            <Investment
                currentLoan={loan}
                makeInvesting={funcMock}
            />
        );
        const loanTitle = screen.getByText('Voluptate et sed tempora qui quisquam.')
        expect(loanTitle).toBeInTheDocument();

        const amountAvailable = screen.getByText(/Amount available: £/i);
        expect(amountAvailable).toHaveTextContent(/Amount available: £11,959/i);

        expect(screen.getByText(/Loan ends in:/i)).toBeInTheDocument();

        expect(screen.getByRole('button', { name: /invest/i })).toBeInTheDocument();
    })

    it('form: happy path', async () => {
        const handleSubmit = jest.fn();

        render(
            <Investment
                currentLoan={loan}
                makeInvesting={handleSubmit}
            />);

        const fieldSum = (screen.getByTestId("field-sum"))
        expect(fieldSum).toHaveValue(null)

        userEvent.type(fieldSum, "123");
        expect(fieldSum).toHaveValue(123);
        const Btn = screen.getByRole('button', { name: /invest/i })
        expect(Btn).not.toBeDisabled();

        userEvent.click(Btn);

        await waitFor(() => {
            expect(handleSubmit).toHaveBeenCalledWith("1", 123);
            expect(handleSubmit).toHaveBeenCalledTimes(1);
        })
     
    });

    it('form: should only accept numbers', () => {
        render(
            <Investment
                currentLoan={loan}
                makeInvesting={funcMock}
            />);
        const fieldSum = (screen.getByTestId("field-sum"))
        expect(fieldSum).toHaveValue(null)

        userEvent.type(fieldSum, "abc");
        expect(fieldSum).toHaveValue(null)
        expect(screen.queryByText("abc")).toBeNull();

        userEvent.type(fieldSum, "12@");
        expect(fieldSum).toHaveValue(12);

        userEvent.type(fieldSum, "%$=!5");
        expect(fieldSum).toHaveValue(125);
    });

});
