/* eslint-disable react/react-in-jsx-scope */
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import LoanComponent from './LoanComponent';

describe('Loan component', () => {

    const clickMock = jest.fn();

    const loan = {
        id: '1',
        title: 'title',
        tranche: 'A',
        available: '42',
        annualized_return: "21.5",
        amount: "85000",
        ltv: "48.80",
        term_remaining: "1656396208000",
    }

    it('should be render happy path', () => {
        const falsyInvested = false

        render(
            <LoanComponent
                loan={loan} onInvest={clickMock} isInvested={falsyInvested}
            />

        );
     
        expect(screen.getByText('title')).toBeInTheDocument();

        const trancheElem = screen.getByText(/Tranche:/i);
        expect(trancheElem).toHaveTextContent(/Tranche: A/i);

        expect(screen.getByText(/Available: £42/i)).toBeInTheDocument();
        expect(screen.getByText(/Annualized return: 21.5%/i)).toBeInTheDocument();
        expect(screen.getByText(/Amount: £85,000/i)).toBeInTheDocument();

        expect(screen.queryByText(/Invested/i)).not.toBeInTheDocument()
    });
    it('Invested: should be render', () => {
        const invested = true;
        render(
            <LoanComponent
                loan={loan} onInvest={clickMock} isInvested={invested}
            />
        );
        
        expect(screen.getByText("Invested")).toBeInTheDocument();
    });

    it('Button onClick', () => {
        const invested = true;
        render(
            <LoanComponent
                loan={loan} onInvest={clickMock} isInvested={invested}
            />
        );

        const Btn = screen.getByRole('button', { name: /invest/i })
        expect(Btn).not.toBeDisabled();

        userEvent.click(Btn);
        expect(clickMock).toHaveBeenCalledWith(loan);
        expect(clickMock).toHaveBeenCalledTimes(1);
    });
});
