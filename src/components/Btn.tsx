import React from 'react';
import styled from 'styled-components';

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> { };

const StyledButton = styled.button<Props>`
  align-items: center;
  height: 46px;
  width: 140px;
  border-radius: 8px;
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  line-height: 14px;
  margin: 5px auto;
  cursor: pointer;
  &:focus {
    outline: none;
  }
  background: yellow;
  border: 1px solid grey;
  &:hover {
    transition: 0.5s;
    background: #ffe600;
    border: 1px solid gold;
  }
`;

const Btn: React.FC<Props> = (props) => {
  const { children, ...restProps } = props;

  return (
    <StyledButton {...restProps}>
      {children}
    </StyledButton>
  );
};

export default Btn;